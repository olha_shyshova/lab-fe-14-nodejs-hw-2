const jwt = require('jsonwebtoken');
const User = require('../models/User.js');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');

class authController {
    async register(req, res) {
        try {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(400).json({message: 'Any field cannot be empty'});
            }

            const userExists = await User.findOne({username: req.body.username});
            if(userExists) {
                return res.status(400).json({message: "Pls choose another name"})
            }

            const hashedPassword = bcrypt.hashSync(req.body.password, 10);
            const user = new User({
                username: req.body.username, 
                password: hashedPassword
            });

            const savedUser = await user.save();
            return res.status(200).json({message: "Success"})
        } catch(e) {       
            res.status(500).json({message: 'Internal server error'})
        }
    }

    async login(req, res) {
        try {
            const {username} = req.body;
            const user = await User.findOne({username});
            if(!user) return res.status(400).json({message: 'There is no such a user(('});
            const validPassword = bcrypt.compareSync(req.body.password, user.password);
            // if(!validPassword) return res.status(400).json({message: "Invalid password"});
            const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, {expiresIn: "12h"});
            res.status(200).json({message: "Success", jwt_token: token});
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }    
}

module.exports = new authController();
