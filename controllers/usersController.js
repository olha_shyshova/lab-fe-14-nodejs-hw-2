const User = require('../models/User.js');
const bcrypt = require('bcryptjs');

class usersController {
  async getProfileInfo(req, res) {
    let user;
    try {
      user = await User.findOne({_id: req.user._id})
    } catch (e) {      
      res.status(500).json({
        message: "Internal server error",
      });
      return;
    }

    res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate
      }
    });

}

  async deleteUser(req, res) {
    try {
      User.findOneAndRemove({_id: req.user._id}, (err) => {
          if(err) {
              res.status(400).json({message: "Can not delete"});
              return;
          }
          res.status(200).json({message: "Success"});
      })
  } catch(e) {
      res.status(500).json({message: "Internal server error"})
  }
 }

  async changePassword(req, res) {
    try {
      const hashPassword = bcrypt.hashSync(req.body.newPassword, 7);
      User.findByIdAndUpdate(req.user.id, {password: hashPassword})
          .then(() => res.status(200).send({message: 'Success'}))
          .catch((e) => {
            res.status(400).json({message: "Cannot delete"});
          })
    } catch (e) {
      res.status(500).json({message: "Internal server error"})
    }
  }
}

module.exports = new usersController();