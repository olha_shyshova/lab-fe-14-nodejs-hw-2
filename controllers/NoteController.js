const Mongoose = require('mongoose');
const ObjectId = Mongoose.Types.ObjectId;
const Note = require('../models/Note');

class notesController {
    async addNote (req, res) {
        try {
            const text = req.body.text;
            const note = new Note({
                userId: new ObjectId(req.user.id),
                completed: false,
                text: text,
                createdDate: new Date().toISOString()
            });
            await note.save()
            res.status(200).json({message: "Success"});
        } catch (e) {
                res.status(500).json({message: "Internal server error"});
        }
    }

    async getNoteById (req, res) {
        const id = req.params.id;

        if (!id) {
            res.status(400).json({ message: "No id"});
        }
        try {
            const note = await Note.findById({ _id: id });

            if (!note) {
                res.status(400).json({ message: "Incorrect note" });
            }

            res.status(200).json({ note });

        } catch (e) {
            res.status(500).json({message: "Internal server error"});
        }
    }

    async updateNoteById (req, res) {
        const id = req.params.id;
        const text = req.body.text;
        if (!id || !text) {
            res.status(400).json({ message: "No id or text"});
        }

        try {
            const note = await Note.findById({ _id: id });
            if (!note) {
                res.status(400).json({ message: "Incorrect note"});
            }

            note.text = text;
            await Note.findByIdAndUpdate({ _id: id }, note);

            res.status(200).json({ message: "Success"});
        } catch (err) {
            res
                .status(500)
                .json({ message: "Internal server error"});
        }

    }

    async deleteNoteById (req, res) {
        if (!req.params.id) {
            res.status(400).json({message: "Bad id"});
        }
        const note = await Note.findById({ _id: req.params.id });
        if (!note) {
            res.status(400).json({message: "Incorrect note"});
        }

        try {
            await Note.findByIdAndDelete({_id: req.params.id})
                res.status(200).json({message: "Note deleted"});
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }

    }

    async checkNote (req, res) {
        const id = req.params.id;
        if (!id) {
            res.status(400).json({ message:"Incorrect id"});
        }

        try {
            const note = await Note.findById({ _id: id });
            if (!note) {
                res.status(400).json({message:"Incorrect note"});
            }

            note.completed = !note.completed;
            await Note.findByIdAndUpdate({ _id: id }, note);

            res.status(200).json({ message: "Success"});
        } catch (err) {
            res.status(500).json({ message: "Internal server error"});
        }
    }

    async getUserNotes(req, res, next) {
        try {
            const {offset, limit} = req.query;
            const perPage = Math.max(0, +limit);
            Note
                .find()
                .skip(+offset || 0)
                .limit(+limit || 0)
                .then((notes) =>
                  res.status(200).send({
                    offset: +offset || 0, limit: +limit || 0, count: Math.floor(perPage / +offset || 0), notes: notes
                  }))
                .catch((e) => {
                    return res.status(400).json({message:"Incorrect note"});
                });
          } catch (e) {
            res.status(500).json({ message: "Internal server error"});
          }
        
      }
}

module.exports = new notesController();

