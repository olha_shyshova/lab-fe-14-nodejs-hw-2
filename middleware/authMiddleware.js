const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
  if (req.method === 'OPTIONS') {
    next()
  } 
 
  try {
    // const token = req.headers.authorization.split(' ')[1];
    const [tokenType, token] = req.headers['authorization'].split(' ');
    if (!token) {
      return res.status(400).json('access denied');
    }
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);    
    req.user = verified;
    next();
  } catch (err) {
    res.status(400).json({message: 'Invalid token'})
  }
};
