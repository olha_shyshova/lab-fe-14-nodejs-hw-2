const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    completed: {
        type: Boolean,
        default: false,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now(),
        required: true
    }


}, {versionKey: false})

const Note = mongoose.model('Note', NoteSchema);
module.exports = Note