const router = require('express').Router();
const usersController = require('../controllers/usersController');
const verify = require('../middleware/authMiddleware');

router.get('/users/me', verify, usersController.getProfileInfo);
router.patch('/users/me', verify, usersController.changePassword);
router.delete('/users/me', verify, usersController.deleteUser);

module.exports = router;
