const router = require('express').Router();
const verify = require('../middleware/authMiddleware');
const notesController = require('../controllers/NoteController');

router.post('/notes', verify, notesController.addNote);
router.get('/notes/:id', verify, notesController.getNoteById);
router.put('/notes/:id', verify, notesController.updateNoteById);
router.delete('/notes/:id', verify, notesController.deleteNoteById);
router.patch('/notes/:id', verify, notesController.checkNote);
router.get('/notes', verify, notesController.getUserNotes);

module.exports = router;
