const express = require('express');
const app = express();
// const dotenv = require('dotenv');
const mongoose = require('mongoose');
require('dotenv').config();
const morgan = require('morgan');
//import Routes
const authRouter = require('./routes/authRouter');
const notesRouter = require('./routes/notesRouter');
const usersRouter = require('./routes/usersRouter');


// dotenv.config();
app.use(morgan('combined'));

//Middleware
app.use(express.json());

//Route middleware
app.use('/api', authRouter);
app.use('/api', notesRouter);
app.use('/api', usersRouter);

const PORT = +process.env.PORT || 8080;
const database = process.env.DB;

//connect to DB
const start = async () => {
    try {
        await mongoose.connect(database,
          { useNewUrlParser: true },
          () => console.log('connected to DB')
        );
        app.listen(PORT, () => console.log(`server runs http://localhost:${PORT}`));
    } catch (e) {
        console.log(e);
    }
}
start();
